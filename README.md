# Introduction
Les cartes flash sont un petit outil utile pour mémoriser des informations, et elles ont été utilisées dès le 19ème siècle.

J'ai créé une application ionic qui utilise le conceptes des cartes de mémoire que ça soit pour des jeux, quizz, ou pour apprendre des nouvelles connaissances

j'ai crée un composant qui s'appelle "flash card" qui me permet d'avoir deux faces front (avec un contenu), et en cliquant l'autre face (back) s'affiche

L'animation CSS utilisée pour ce composant a été créée par [David Walsh](https://davidwalsh.name/css-flip) . Un grand merci à lui pour cela

# Création de l'application

1. __démarrer l'application__ grace à :
`ionic start (nom de l'application)`
1. __générer un composant__ en exécutant la commande : `ionic g component FlashCard` . 
cela générera automatiquement les fichiers nécessaires à notre composant et les placera dans le dossier `components` . Enfin, nous devons également configurer ce nouveau composant dans notre fichier  `app.modules.ts` 
1. __Créer le modèle du composant__ : C'est le code HTML qui va être injecté dans notre application où que notre `<flash-card>`
    1. j'ai créé la fonction `flip()` qui bascule selon le click
    1. la balise `<ng-content>` nous permet d'injecter du contenu dynamique grace à un sélecteur (par class)
1. __Récupérer les donnes depuis le web service__ : afin de récupérer les données d'u 
ne source externe (Rest API), j'ai utilisé le fake server [`json-server`](https://github.com/typicode/json-server)
    1. le message du web service est un tableau d'objects qui ont cette structure :
    ```
    {
      "id": "1",
      "category": "presidents", //on peut avoir plusieurs catégories 
      "front": "QUESTION",
      "front_info": "information",
      "back": "RESPONSE",
      "back_info": "information"
    },
    ``` 

    1. on génère un provider en executant : `ionic g provider cardsDataProvider`
    1. l'url de pour spécifier l'app est dans le fichier : __`src/providers/cards-data/cards-data.ts`__ (dans la variable : `dataSource`)
    1. on parcours les datas dans notre `cardsDataProvider` via la fonction `getRemoteData`
    1. on utilise dans notre page de vue (home.html) une boucle de flash cards grace à l'attribut Angular `*ngFor` pour afficher toutes les cartes résultats de notre web service
    
# DEMO
![demo](assets/demo.gif "demo")

# Author

[Aymene Bourafai](https://aymenebourafai.com/) 

(2018 - 2019)
