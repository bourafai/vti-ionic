import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {CardsDataProvider} from "../../providers/cards-data/cards-data";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  flashCards: any;
  constructor(public navCtrl: NavController, public cardsService: CardsDataProvider ) {

  }

  ionViewDidLoad(){
    console.log(this.cardsService.getRemoteData().subscribe(data=> {
      this.flashCards = data;
    }));
  }

}
